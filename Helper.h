/*
 * Helper.h
 *
 *  Created on: 10.10.2018
 *      Author: CHartinger
 */

#ifndef HEADER_HELPER_H_
#define HEADER_HELPER_H_


namespace helper {

int getRandomInt(int start, int end);
float getUnivRandomFloat(float start, float end);
float getGaussRandomFloat(float mean,float stddev);
float getGaussRandomFloat(float mean,float stddev,float max);
}
#endif /* HEADER_HELPER_H_ */
