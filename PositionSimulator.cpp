/*
 * PositionSimulator.cpp
 *
 *  Created on: 25.01.2019
 *      Author: CHartinger
 */

#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <string>

#include "zmq.hpp"

#include "Player.h"
#include "PosSim.pb.h"
#include "Helper.h"

using namespace std;
using namespace std::chrono;


int main(int nArgs, char* args[]) {
	GOOGLE_PROTOBUF_VERIFY_VERSION;

	int howLong=600;
	int port=7777;

	if(nArgs==3){
		int howLong_=atoi(args[1]);
		int port_=atoi(args[2]);
		howLong=(howLong_>0)?howLong_:howLong;
		port=(port_>1023&&port_<65535)?port_:port;

	}
	else{
		cout <<"Use PosSim <seconds> <port> to change the default values."<<endl;
	}

	cout <<"Will send messages for "<<howLong<<" seconds. Connect on port "<<port<<" to receive them."<<endl;


	string bindAddress("tcp://*:");
	bindAddress=bindAddress+to_string(port);

	zmq::context_t context(1);
	zmq::socket_t publisher(context,ZMQ_PUB);

	publisher.bind(bindAddress.c_str());

	vector<Player>players;
	for (int i=0;i<10;i++) {
		players.push_back(Player(i));
	}

	while(howLong>0) {

		for(Player& player:players) {
			player.updatePosition();

			GeneratedPosition posMessage;
			posMessage.set_sensorid(player.getId());
			microseconds time=duration_cast<microseconds>(system_clock::now().time_since_epoch());
			posMessage.set_timestamp_usec(time.count());

			Data3d* playerPos=posMessage.mutable_position();
			playerPos->set_x(player.getXPos()+helper::getGaussRandomFloat(0.0,0.15,0.30));
			playerPos->set_y(player.getYPos()+helper::getGaussRandomFloat(0.0,0.15,0.30));
			playerPos->set_z(player.getZPos()+helper::getGaussRandomFloat(0.0,0.15,0.30));

			int messageSize=posMessage.ByteSize();
			char serializationArray[messageSize];
			posMessage.SerializeToArray(serializationArray,messageSize);

			publisher.send(serializationArray,messageSize);


			this_thread::sleep_for(milliseconds(100));

		}

		cout<<"Sent ten messages."<<endl;
		howLong--;
	}

	cout<<"Goodby"<<endl;
	return 0;
}


//testreceiver
/*
int main(int nArgs, char** args){

	void *context=zmq_ctx_new();
	void *subscriber=zmq_socket(context, ZMQ_SUB);

	zmq_connect(subscriber,"tcp://localhost:7777");
	zmq_setsockopt(subscriber,ZMQ_SUBSCRIBE,"",strlen(""));

	char receiveBuffer[1000];

	while (true) {
		int received = zmq_recv(subscriber,receiveBuffer, 100,0);

		GeneratedPosition posMessage;
		posMessage.ParseFromArray(receiveBuffer, 1000);

		cout <<"ID: "<<posMessage.sensorid()<<" ts: "<<posMessage.timestamp_usec()<<" x:"<<posMessage.position().x()<<" y:"<<posMessage.position().y()<<endl;
	}
}*/

