/*
 * Player.h
 *
 *  Created on: 25.01.2019
 *      Author: CHartinger
 */

#ifndef PLAYER_H_
#define PLAYER_H_

class Player {
public:
	Player(int id_);
	virtual ~Player();
	void updatePosition();
	int getId();
	float getXPos();
	float getYPos();
	float getZPos();
private:
	int id;
	float currentSpeed;
	float currentAngle;
	float xPos;
	float yPos;
	float zPos;
};

#endif /* PLAYER_H_ */
