/*
 * Player.cpp
 *
 *  Created on: 25.01.2019
 *      Author: CHartinger
 */

#include "Player.h"
#include "Helper.h"
#include <cmath>

const float leftBorder = 0.0;
const float rightBorder = 100.0;
const float upperBorder = 100.0;
const float lowerBorder = 0.0;

Player::Player(int id_) :
	id(id_) {
	xPos = helper::getUnivRandomFloat(leftBorder, rightBorder);
	yPos = helper::getUnivRandomFloat(lowerBorder, upperBorder);
	zPos = 1.5;
	currentAngle = helper::getUnivRandomFloat(0.0, 2*M_PI);
	currentSpeed = helper::getUnivRandomFloat(2.0, 8.0);
}

Player::~Player() {
	// TODO Auto-generated destructor stub
}

void Player::updatePosition() {
	//change direction and speed based on random value
	if (helper::getUnivRandomFloat(0.0, 1.0) > 0.85) {
		currentAngle = helper::getUnivRandomFloat(0.0, 2*M_PI);
	}
	if (helper::getUnivRandomFloat(0.0, 1.0) > 0.85) {
		currentSpeed = helper::getUnivRandomFloat(2.0, 8.0);
	}
	//update position based on current values
	xPos += cos(currentAngle) * currentSpeed;
	yPos += sin(currentAngle) * currentSpeed;
	//check if on border
	xPos=(xPos<leftBorder)?leftBorder:xPos;
	xPos=(xPos>rightBorder)?rightBorder:xPos;
	yPos=(yPos<lowerBorder)?lowerBorder:yPos;
	yPos=(yPos>upperBorder)?upperBorder:yPos;

	if(xPos==leftBorder){
		currentAngle=helper::getUnivRandomFloat(-0.5*M_PI,0.5*M_PI);
	}
	if(xPos==rightBorder){
		currentAngle=helper::getUnivRandomFloat(0.5*M_PI,1.5*M_PI);
	}
	if(yPos==lowerBorder){
		currentAngle=helper::getUnivRandomFloat(0.0,M_PI);
	}
	if(yPos==upperBorder){
		currentAngle=helper::getUnivRandomFloat(M_PI,2.0*M_PI);
	}

}

int Player::getId(){
	return id;
}
float Player::getXPos(){
	return xPos;
}
float Player::getYPos(){
	return yPos;
}
float Player::getZPos(){
	return zPos;
}
