/*
 * Helper.cpp
 *
 *  Created on: 16.10.2018
 *      Author: CHartinger
 */

#include "Helper.h"

#include <iostream>
#include <random>
#include <chrono>



namespace helper {

using namespace std;

unsigned seed = chrono::system_clock::now().time_since_epoch().count();
//unsigned seed = 42;
default_random_engine generator(seed);


int getRandomInt(int start, int end) {
	uniform_int_distribution<int> unifDist(start, end);
	return unifDist(generator);
}

float getUnivRandomFloat(float start, float end){
	uniform_real_distribution<float> unifDist(start,end);
	return unifDist(generator);
}

float getGaussRandomFloat(float mean,float stddev){
	std::normal_distribution<float> normalDist(mean, stddev);
	return normalDist(generator);
}


float getGaussRandomFloat(float mean,float stddev,float max){
	float res=getGaussRandomFloat(mean,stddev);
	res=(res<-max)?-max:res;
	res=(res>max)?max:res;
	return res;
}



}
